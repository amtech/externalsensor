package mx.amtech.mobile.myexternalsensor;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

import android.os.DeadObjectException;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;

import androidx.navigation.ui.AppBarConfiguration;

import mx.amtech.mobile.myexternalsensor.databinding.ActivityMainBinding;
import mx.amtech.mobile.proximitygateway.services.IAmtechCommunicationCallbacks;
import mx.amtech.mobile.proximitygateway.services.IAmtechCommunicationInterface;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {
    private static final String LOG_TAG = "SCLIENTAPP";

    private AppBarConfiguration appBarConfiguration;
    private ActivityMainBinding binding;
    private String connToken = null;
    private String connTokenGps = null;
    private IAmtechCommunicationInterface service = null;
    private TextView textView = null;
    boolean connected = false;
    private IAmtechCommunicationCallbacks callbacks = new IAmtechCommunicationCallbacks.Stub() {

        @Override
        public void onSettingsChanged(String jsonAsString) throws RemoteException {
            setText("Settings changed");
        }

        @Override
        public void onProcessStopped() throws RemoteException {
            setText("Process stopped");
        }

        @Override
        public void onProcessStarted() throws RemoteException {
            setText("Process started");
        }
    };

    void toggleConnected(boolean connected) {
        this.connected = connected;
        binding.btnDisconnect.setVisibility(connected ? View.VISIBLE : View.GONE);
        binding.btnConnect.setVisibility(connected ? View.GONE : View.VISIBLE);
    }

    boolean connect() {
        try {
            String token = service.connect("amtechdemofollower@amtech.mx", "amtechdemofollower", "Test1234", "cv");
            if (token != null) {
                connToken = token;
                service.registerCallbacks(connToken, callbacks);
                return true;
            }

        } catch (DeadObjectException ex) {
            Log.e(LOG_TAG, "DeadObjectException");
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return false;

    }

    class AmtechCommunicationConnection implements ServiceConnection {
        public void onServiceConnected(ComponentName className,
                                       IBinder boundService) {
            service = IAmtechCommunicationInterface.Stub.asInterface((IBinder) boundService);
            Log.d(LOG_TAG, "onServiceConnected");
            toggleConnected(true);
            if (connect()) {
                setText("Service connected");
            } else {
                setText("The service or the process is not available");
            }
        }

        public void onServiceDisconnected(ComponentName className) {

            setText("Service disconnected abruptly");
            onDisconnected();
        }
    }

    private final AmtechCommunicationConnection conn = new AmtechCommunicationConnection();

    private void initService() {

        Intent it = new Intent();
        Package pack = IAmtechCommunicationInterface.class.getPackage();
        Log.i(LOG_TAG, "Preparing to bind to service");
        if (pack != null) {

            it.setAction("IAmtechCommunicationInterface");
            String packageName = Objects.requireNonNull(pack).getName();
            it.setPackage(packageName.substring(0, packageName.lastIndexOf(".")));
            //it.setClassName("mx.amtech.mobile.proximitygateway",pack.getName()+".AmtechCommunicationService");

            Log.i(LOG_TAG, "Service is going to start");
            boolean res = bindService(it, conn, Context.BIND_AUTO_CREATE);

            Log.i(LOG_TAG, "Service is started?");
            Log.i(LOG_TAG, "binding service returned: " + res);
        }

    }

    private void onDisconnected() {
        toggleConnected(false);
        service = null;
        connToken = null;
    }

    private void releaseService() {
        try {
            if (service != null) {
                setText("Disconnecting from service");
                service.disconnect(connToken);
                unbindService(conn);
                onDisconnected();
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }

    }

    static final String PLACEHOLDE_TYPE = "${deviceType}";
    static final String PLACEHOLDE_NAME = "${deviceName}";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        textView = binding.intentReceiver;
        binding.btnConnect.setOnClickListener(view -> initService());
        binding.btnDisconnect.setOnClickListener(view -> releaseService());
        toggleConnected(false);
        binding.fab.setOnClickListener(view -> Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Send obs", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {
                            if (connToken == null && !connect()) {
                                sendToast("The process is not available");
                            } else {
                                /*
                                - type
                                - label
                                - company
                                - imagename
                                 */
                                JSONObject obs = new JSONObject();
                                obs.put("type", "alert")
                                        .put("@type","cvAlertObs")
                                        .put("label", "label")
                                        .put("company", "company")
                                        .put("imagename", "imagename");

                                service.sendObservation(connToken, obs.toString());
                                sendToast("observation sent");

                            }
                        } catch (RemoteException | JSONException remoteException) {
                            remoteException.printStackTrace();

                        }
                    }
                }).show());

        setText("Waiting for connection");

        initService();

    }

    void sendToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    void setText(String message) {
        Log.i(LOG_TAG, message);
        textView.setText(message);
        sendToast(message);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}