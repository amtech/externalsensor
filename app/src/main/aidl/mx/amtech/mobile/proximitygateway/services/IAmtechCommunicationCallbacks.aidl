// IAmtechCommunicationCallbacks.aidl
package mx.amtech.mobile.proximitygateway.services;

// Declare any non-default types here with import statements

interface IAmtechCommunicationCallbacks {
    /**
     * Notifies that the settings have changed
     */
    void onSettingsChanged(String jsonAsString)=0;
    void onProcessStopped()=1;
    void onProcessStarted()=2;

}