// IAmtechCommunicationInterface.aidl
package mx.amtech.mobile.proximitygateway.services;
import mx.amtech.mobile.proximitygateway.services.IAmtechCommunicationCallbacks;

// Declare any non-default types here with import statements

interface IAmtechCommunicationInterface {
    /**
     * Returns a process token to be used in the future comunications
     */
    String connect(String username, String tenant, String password, String processId)=1;
    void disconnect(String processToken)=2;

    /**
     * Tells the service to send the given observation. Must be a stringified JSONObject
     */
    boolean sendObservation(String connectionToken,  String obsAsString)=11;
    void registerCallbacks(String connectionToken, IAmtechCommunicationCallbacks callback)=12;
    void unregisterCallbacks(String connectionToken)=13;

}